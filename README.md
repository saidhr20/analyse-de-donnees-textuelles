# Analyse de données textuelles
**Sentiment Analysis Text Classification using :**

-Bag of Words with K-Nearest Neighbor Algorithm
-TF-IDF model with K-Nearest Neighbor Algorithm

This project determines a postive or negative sentiment based on the review text and utilizes either the bag of words model or tf-idf model in K Nearest Neighbor classification. this projet was made for homework at Saad dahlab University TAL .

This code uses Python 3.8.5 and I used Jupyter Notebook as the IDE. Both the project interpreter default settings and the configurations must be in Python 3.8.5. Several modules may need to be imported including: nltk, numpy, pandas,scikit-learn, scipy, and sklearn.

**preporcessing**:
-tokenistaion
-remove_stopwords
-lemmatize
-stemmer

i used 50,000 records from IMDb to convert each review into a ‘TF IDF’ and ‘bag of words’, which i used in a KNN Algorithm.
 This is a classification algorithm based on the k-nearest neighbors as machine learning model.
 Both BOW and TFIDF methods must specify the name of the .csv file to open in training_data .

Variables that can be changed in both dictionary models: remove_stopwords, lemmatize, stemmer

function review_similair in TF IDF  allows you to search for a sentence to give the five most
similar from the database.

** End Notes**

** KNN + Bow  results :**

   precision    recall  f1-score   support

    negative       0.89      0.83      0.86     25000
    positive       0.84      0.90      0.87     25000

    accuracy                           0.86     50000
   macro avg       0.87      0.86      0.86     50000
weighted avg       0.87      0.86      0.86     50000

** KNN+ TF IDF results :**

            precision    recall  f1-score   support

           0       0.80      0.69      0.74      7540
           1       0.72      0.83      0.77      7460

    accuracy                           0.76     15000
   macro avg       0.76      0.76      0.76     15000
weighted avg       0.76      0.76      0.76     15000


Conclution :

_**Bag of word** precision and recall, F1 score reaches the best value as TF IDF, that we could notice that Bow had the best training model  _**> **


References:
https://www.coursera.org/learn/language-processing/home


